import Modal from '@typo3/backend/modal.js';

class lfEditor {
    constructor() {
        this.hideAll = null;
        /**
         * Indicates if untranslated languages are hidden.
         * @type {boolean}
         */
        this.untranslatedAreHidden = false;

        /**
         * Index of column which contains state of languages.
         * @type {number}
         */
        this.STATE_COLUMN_INDEX = 1;

        /**
         * Index of span element which contains number of translated constants.
         * @type {number}
         */
        this.TRANSLATED_SPAN_INDEX = 1;
        this.init();
    }

    init() {
        document.onkeydown = this.saveOnKeyDown;
        const searchResults = document.querySelectorAll('.tx-lfeditor-search-result');
        for (const searchResult of searchResults) {
            const searchStringEscaped = searchResult.dataset.searchString.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
            let mainContent = searchResult.innerText;
            let flags = 'g';
            if (!document.getElementById('caseSensitive').checked) {
                flags += 'i';
            }

            // Perform a case-insensitive search and wrap occurrences with the highlight class
            const regex = new RegExp(`(${searchStringEscaped})`, flags);
            const highlightedContent = mainContent.replace(regex, '<span class="tx-lfeditor-highlighted-search">$1</span>');
            searchResult.innerHTML = highlightedContent;
        }

        const changeLanguageFileSelects = document.querySelectorAll('.change-language-file');
        for (const changeLanguageFileSelect of changeLanguageFileSelects) {
            changeLanguageFileSelect.addEventListener('change', (event) => {
                this.jump(event.currentTarget);
            });
        }

        const formSubmitButtons = document.querySelectorAll('.form-submit-btn');
        for (const formSubmitButton of formSubmitButtons) {
            formSubmitButton.addEventListener('click', (event) => {
                const formId = event.currentTarget.dataset.formId;
                document.getElementById(formId).submit();
                event.preventDefault();
            });
        }

        const submitLanguageFileEditButtons = document.querySelectorAll('.submit-language-file-edit-btn');
        for (const submitLanguageFileEditButton of submitLanguageFileEditButtons) {
            submitLanguageFileEditButton.addEventListener('click', (event) => {
                event.preventDefault();
                const buttonType = submitLanguageFileEditButton.dataset.type;
                if (buttonType === 'cancel') {
                    this.confirmCancelFileEdit();
                    return;
                }

                this.submitLanguageFileEdit(buttonType);
            });
        }

        const treeEntryButtons = document.querySelectorAll('.tree-entry-btn');
        for (const treeEntryButton of treeEntryButtons) {
            treeEntryButton.addEventListener('click', (event) => {
                event.preventDefault();
                const uri = event.currentTarget.dataset.uri,
                    isBottom = event.currentTarget.dataset.isBottom,
                    constantKey = event.currentTarget.dataset.constantKey;
                this.openCloseTreeEntry(uri, 'ul-' + constantKey, 'icon-' + constantKey, isBottom);
            });
        }

        const treeEntryIcons = document.querySelectorAll('.tree-entry-icon');
        for (const treeEntryIcon of treeEntryIcons) {
            if (typeof treeEntryIcon.dataset.marginLeft !== 'undefined') {
                treeEntryIcon.style = treeEntryIcon.dataset.marginLeft + 'px';
            }
        }

        const toggleUntranslatedButtons = document.querySelectorAll('.toggle-untranslated-btn');
        for (const toggleUntranslatedButton of toggleUntranslatedButtons) {
            toggleUntranslatedButton.addEventListener('click', (event) => {
                event.preventDefault();
                this.hideShowUntranslatedLanguagesInTable();
            });
        }

        const toggleAllButtons = document.querySelectorAll('.toggle-all-btn');
        for (const toggleAllButton of toggleAllButtons) {
            toggleAllButton.addEventListener('click', (event) => {
                event.preventDefault();
                this.hideUnHideAll();
            });
        }

        const jumpButtons = document.querySelectorAll('.lfeditor-jump');
        for (const jumpButton of jumpButtons) {
            jumpButton.addEventListener('change', (event) => {
                this.jump(event.currentTarget);
                event.preventDefault();
            });
        }

        const changeFormButtons = document.querySelectorAll('.change-form-btn');
        for (const changeFormButton of changeFormButtons) {
            changeFormButton.addEventListener('change', (event) => {
                this.changeForm(event.currentTarget.dataset.formId);
            });
        }

        this.initHideShowFunctionality();
    }

    submitLanguageFileEdit(buttonType) {
        const form = document.forms.contentForm;
        const buttons = form.querySelectorAll("[name$='buttonType']");
        for (const button of buttons) {
            button.value = buttonType;
        }

        form.submit();
    }

    /**
     * Renders confirmation dialog for cancel button.
     *
     * @returns {boolean}
     */
    confirmCancelFileEdit() {
        let modal = Modal.confirm(
            TYPO3.lang['function.langfile.confirmCancel.title'],
            TYPO3.lang['function.langfile.confirmCancel']
        );
        modal.addEventListener('confirm.button.ok', () => {
            Modal.dismiss();
            this.submitLanguageFileEdit(-1);
        });
        modal.addEventListener('confirm.button.cancel', () => {
            Modal.dismiss();
        });
        return false;
    }

    openCloseTreeEntry(prefix, fieldId, picId, bottom) {
        const fieldElement = document.getElementById(fieldId);
        const picElement = document.getElementById(picId);
        let pic, curTreeHide;

        curTreeHide = 0;
        if (!fieldElement.classList.contains('hidden')) {
            curTreeHide = 1;
        }

        if (curTreeHide) {
            fieldElement.classList.add('hidden');
            pic = 'Plus';
        } else {
            fieldElement.classList.remove('hidden');
            pic = 'Minus';
        }

        if (bottom) {
            pic = pic + 'Bottom';
        }

        picElement.src = prefix + '/tree' + pic + '.png';
        picElement.alt = 'tree' + pic + '.png';
    }

    /**
     * Folds and un folds all constants in tree, on tree view.
     */
    hideUnHideAll() {
        if (this.hideAll === null) {
            this.hideAll = !document.getElementById('ul-Root').classList.contains('hidden');
        }

        const ulIdRegex = /^ul-/;
        const treeUlElements = [];
        const allUl = document.getElementsByTagName('ul');
        for (const ulElement of allUl) {
            if (ulIdRegex.test(ulElement.id)) {
                treeUlElements.push(ulElement);
            }
        }

        const imageIdRegex = /^icon-/,
            imageMinusSrcRegex = /treeMinus/,
            imagePlusSrcRegex = /treePlus/,
            treeImgMinusElements = [],
            treeImgPlusElements = [],
            allImg = document.getElementsByTagName('img');
        for (const imageElement of allImg) {
            if (imageIdRegex.test(imageElement.id)) {
                if (imageMinusSrcRegex.test(imageElement.src)) {
                    treeImgMinusElements.push(imageElement);
                } else if (imagePlusSrcRegex.test(imageElement.src)) {
                    treeImgPlusElements.push(imageElement);
                }
            }
        }

        if (this.hideAll) {
            for (const treeUlElement of treeUlElements) {
                treeUlElement.classList.add('hidden');
            }

            for (const treeImgMinusElement of treeImgMinusElements) {
                treeImgMinusElement.src = treeImgMinusElement.src.replace(imageMinusSrcRegex, 'treePlus');
            }

            this.hideAll = false;
        } else {
            for (const treeUlElement of treeUlElements) {
                treeUlElement.classList.remove('hidden');
            }

            for (const treeImgPlusElement of treeImgPlusElements) {
                treeImgPlusElement.src = treeImgPlusElement.src.replace(imagePlusSrcRegex, 'treeMinus');
            }

            this.hideAll = true;
        }
    }

    /**
     * Triggers click on button with id = 'tx-lfeditor-button-submit' when user presses Ctrl + Enter.
     *
     * @param eventParameter
     * @returns void
     */
    saveOnKeyDown(eventParameter) {
        const eventObject = window.event ? event : eventParameter;
        if (eventObject.keyCode == 13 && eventObject.ctrlKey) {
            document.getElementById('contentForm').submit();
        }
    }

    changeForm(id) {
        let modal = Modal.confirm(
            TYPO3.lang['function.langfile.confirmChange.title'],
            TYPO3.lang['function.langfile.confirmChange']
        );
        modal.addEventListener('confirm.button.ok', () => {
            Modal.dismiss();
            document.getElementById(id).submit();
        });
        modal.addEventListener('confirm.button.cancel', () => {
            Modal.dismiss();
        });
        return false;
    }

    jump(select) {
        let modal = Modal.confirm(
            TYPO3.lang['function.langfile.confirmChange.title'],
            TYPO3.lang['function.langfile.confirmChange']
        );
        modal.addEventListener('confirm.button.ok', () => {
            Modal.dismiss();
            window.location.href = select.options[select.selectedIndex].value;
        });
        modal.addEventListener('confirm.button.cancel', () => {
            Modal.dismiss();
        });
    }

    /**
     * Gets list of table rows.
     * @returns {NodeList|*}
     */
    getTableRowElements() {
        const tableElement = document.getElementById('tx-lfeditor-table');
        const tableBodyElement = tableElement ? tableElement.getElementsByTagName('tbody')[0] : null;
        const tableRowElements = tableBodyElement ? tableBodyElement.getElementsByTagName('tr') : null;
        return tableRowElements;
    }

    /**
     * This function hides table rows which contain untranslated languages.
     * @returns boolean
     */
    hideUntranslatedLanguagesInTable() {
        let thereAreHiddenElements = false;
        const tableRowElements = this.getTableRowElements();
        if (tableRowElements === null) {
            return thereAreHiddenElements;
        }

        for (const tableRowElement of tableRowElements) {
            const tableDataElements = tableRowElement.getElementsByTagName('td');
            if (tableDataElements === null || typeof tableDataElements[this.STATE_COLUMN_INDEX] === 'undefined') {
                return thereAreHiddenElements;
            }

            const tableDataSpanElements = tableDataElements[this.STATE_COLUMN_INDEX].getElementsByTagName('span');
            if (
                !tableDataSpanElements ||
                typeof tableDataSpanElements[this.TRANSLATED_SPAN_INDEX] === 'undefined' ||
                !tableDataSpanElements[this.TRANSLATED_SPAN_INDEX]
            ) {
                return thereAreHiddenElements;
            }

            const numberTranslated = tableDataSpanElements[this.TRANSLATED_SPAN_INDEX].innerText.trim();
            if (numberTranslated !== '0') {
                continue;
            }

            tableRowElement.classList.add('hidden');
            this.untranslatedAreHidden = true;
            thereAreHiddenElements = true;
        }

        return thereAreHiddenElements;
    }

    /**
     * Reveals hidden table rows.
     * @returns void
     */
    showUntranslatedLanguagesInTable() {
        const tableRowElements = this.getTableRowElements();
        if (tableRowElements === null) {
            return;
        }

        for (const tableRowElement of tableRowElements) {
            if (tableRowElement.classList.contains('hidden')) {
                tableRowElement.classList.remove('hidden');
            }
        }

        this.untranslatedAreHidden = false;
    }

    /**
     * Hides or un hides rows of table which contain untranslated languages.
     * @returns void
     */
    hideShowUntranslatedLanguagesInTable() {
        if (this.untranslatedAreHidden) {
            this.showUntranslatedLanguagesInTable();
        } else {
            this.hideUntranslatedLanguagesInTable();
        }
    }

    initHideShowFunctionality() {
        const thereAreHiddenElements = this.hideUntranslatedLanguagesInTable();
        if (!thereAreHiddenElements) {
            const hideShowLinkElement = document.getElementById('hideShowUntranslatedLanguagesInTableId');
            if (hideShowLinkElement !== null) {
                hideShowLinkElement.classList.add('hidden');
            }
        }
    }
}

export default new lfEditor();
