﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

Editing modes
-------------

There are two editing modes:

- Extension mode
- Override mode

They can be chosen from rightmost select menu on top of the screen.
The select menu is visible only for administrators. If user is not admin, than override mode is chosen by default.
For administrators, default is extension mode.

Extension mode
^^^^^^^^^^^^^^

This mode is useful for extension developers because in this mode, LFEditor edits extension files directly.

Even if copies of extension files exist in l10n folder, or extension files are overridden,
user will still edit extension files only.

Override mode
^^^^^^^^^^^^^

Purpose of this mode is making translations resistant to changes in extension
(e.g. when extension updates, translations will be preserved). Thus, this mode is useful for translators,
and it is set as default and only mode for non-admin users.

- Whenever user makes any change in some language file, only changed constants (or meta data) will be saved in corresponding language file in  'config\lfeditor\OverrideFiles'.

- When reading language file, LFEditor is first reading constants from files in override folder, then it reads rest of constants from l10n folder (if there is corresponding file in l10n folder), and then reads from ext folder (if there was no file in l10n folder).

.. important::
	All the changes (edit/add/delete/rename constant) to language file will be saved in override files only.
	Original extension files will stay unchanged.
