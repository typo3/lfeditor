# Version 8 Breaking Changes

- Dropped TYPO3 10 and 11 support
- Dropped php 7 support
- Manage Backups module removed
- View Tree module removed
- Change Lfeditor.js from requireJS to JavaScript module
- New location for the locallang overrides. You must move your override files from `typo3conf/LfEditor` to the new
  location in `public/lfeditor`. You must also update the `$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']`
  entries for your overrides to find the now moved files (you mist prefix them with the public-path aka
  `\TYPO3\CMS\Core\Core\Environment::getPublicPath()`). Also consider extracting that from `AdditionConfiguration.php`
  or `additional.php` to a separate `lfeditor.php` file in `config/system/lfeditor.php`, since this is the default
  location for these entries from this version on (can be configured to be something else still in extension settings)

# Version 7 Breaking Changes

- Dropped TYPO3 9 support
- Dropped php 7.3 support
- added support for v4 of typo3/cms-composer-installers

We had to move to .xlf files for overwrites. It is necessary for you to save all overwritten files again from the
backend.

# Version 6 Breaking Changes

- Dropped TYPO3 8 support
