<?php

use SGalinski\Lfeditor\Service\LicensingService;

return [
    'lfeditor::ajaxPing' => [
        'path' => '/lfeditor/ajaxPing',
        'target' => LicensingService::class . '::ajaxPing',
    ],
];
