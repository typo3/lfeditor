<?php

use SGalinski\Lfeditor\Controller\AddConstantController;
use SGalinski\Lfeditor\Controller\DeleteConstantController;
use SGalinski\Lfeditor\Controller\EditConstantController;
use SGalinski\Lfeditor\Controller\EditFileController;
use SGalinski\Lfeditor\Controller\GeneralController;
use SGalinski\Lfeditor\Controller\RenameConstantController;
use SGalinski\Lfeditor\Controller\SearchConstantController;
use SGalinski\Lfeditor\Utility\ExtensionUtility;

$extConf = ExtensionUtility::getExtensionConfiguration();

return [
    'user_lfeditor' => [
        'parent' => $extConf['beMainModuleName'] ?? 'user',
        'position' => ['after' => ''],
        'access' => 'user',
        'workspaces' => 'live',
        'path' => '/module/user/lfeditor',
        'labels' => 'LLL:EXT:lfeditor/Resources/Private/Language/locallang_mod.xml',
        'iconIdentifier' => 'extension-lf_editor-ext-icon',
        'extensionName' => 'Lfeditor',
        'controllerActions' => [
            GeneralController::class => [
                'index',
                'general',
                'changeSelection',
                'generalSave',
                'goToEditFile',
                'switchEditingMode',
                'refreshLanguageFileList',
            ],
            EditFileController::class => [
                'editFile',
                'changeSelection',
                'editFileSave',
                'refreshLanguageFileList',
            ],
            EditConstantController::class => [
                'editConstant',
                'changeSelection',
                'editConstantSave',
                'prepareEditConstant',
                'refreshLanguageFileList',
            ],
            AddConstantController::class => [
                'addConstant',
                'changeSelection',
                'addConstantSave',
                'refreshLanguageFileList',
            ],
            DeleteConstantController::class => [
                'deleteConstant',
                'changeSelection',
                'deleteConstantSave',
                'refreshLanguageFileList',
            ],
            RenameConstantController::class => [
                'renameConstant',
                'changeSelection',
                'renameConstantSave',
                'refreshLanguageFileList',
            ],
            SearchConstantController::class => [
                'searchConstant',
                'changeSelection',
                'searchConstantSearch',
                'refreshLanguageFileList',
                'searchGlobal',
                'searchGlobalSearch',
                'globalSearchToEditConstant',
            ],
        ],
    ],
];
