<?php

use TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider;

/**
 * Important! Do not return a variable named $icons, because it will result in an error.
 * The core requires this file and then the variable names will clash.
 * Either use a closure here, or do not call your variable $icons.
 */
return [
    'extension-lf_editor-ext-icon' => [
        'provider' => SvgIconProvider::class,
        'source' => 'EXT:lfeditor/Resources/Public/Icons/Extension.svg',
    ],
];
