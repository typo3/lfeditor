<?php

use SGalinski\Lfeditor\Service\ConfigurationService;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();
    $services->defaults()
        ->private()
        ->autowire()
        ->autoconfigure();

    $services->set(ConfigurationService::class)->public();

    $services->load('SGalinski\\Lfeditor\\', __DIR__ . '/../Classes/');
};
