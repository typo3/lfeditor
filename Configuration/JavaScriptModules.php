<?php

return [
    'dependencies' => [
        'core',
        'backend',
    ],
    'imports' => [
        '@sgalinski/lfeditor/' => 'EXT:lfeditor/Resources/Public/JavaScript/',
    ],
];
