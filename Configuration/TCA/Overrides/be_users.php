<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

$fieldDefinition = [
    'lfeditor_change_editing_modes' => [
        'exclude' => true,
        'label' => 'LLL:EXT:lfeditor/Resources/Private/Language/locallang_mod.xlf:mlang_tabs_tab',
        'config' => [
            'type' => 'check',
            'items' => [
                '1' => [
                    'label' => 'LLL:EXT:lfeditor/Resources/Private/Language/locallang_mod.xlf:settings_canChangeEditingMode',
                ],
            ],
        ],
        'displayCond' => 'FIELD:admin:REQ:FALSE',
    ],
];

ExtensionManagementUtility::addToAllTCAtypes(
    'be_users',
    'lfeditor_change_editing_modes',
    '',
    'after:allowed_languages'
);

ExtensionManagementUtility::addTCAcolumns('be_users', $fieldDefinition);

ExtensionManagementUtility::addFieldsToPalette(
    'be_users',
    'lfeditor',
    'lfeditor_change_editing_modes',
    'after:allowed_languages'
);
