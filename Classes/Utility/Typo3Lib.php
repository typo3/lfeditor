<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\Lfeditor\Utility;

use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * includes special typo3 methods
 */
class Typo3Lib
{
    /**
     * checks the file location type
     *
     * @param string $file
     * @return string
     */
    public static function checkFileLocation($file): string
    {
        $pathExtensions = Environment::getExtensionsPath() . '/';

        if (str_contains($file, $pathExtensions)) {
            return 'local';
        }

        $pathSysExtensions = Environment::getFrameworkBasePath() . '/';
        if (str_contains($file, $pathSysExtensions)) {
            return 'system';
        }

        $pathL10N = Environment::getLabelsPath() . '/';
        if (str_contains($file, $pathL10N)) {
            return 'l10n';
        }

        if (str_contains($file, 'vendor')) {
            return 'vendor';
        }

        return '';
    }

    /**
     * @param string $fileRef Absolute path of the language file
     * @return string Absolute prefix to the language file location
     */
    public static function getLocalizedFilePrefix($fileRef): string
    {
        // Analyze file reference
        if (\str_starts_with($fileRef, Environment::getFrameworkBasePath() . '/')) {
            // Is system
            $validatedPrefix = Environment::getFrameworkBasePath() . '/';
        } elseif (\str_starts_with($fileRef, Environment::getBackendPath() . '/ext/')) {
            // Is global
            $validatedPrefix = Environment::getBackendPath() . '/ext/';
        } elseif (\str_starts_with($fileRef, Environment::getExtensionsPath() . '/')) {
            // Is local
            $validatedPrefix = Environment::getExtensionsPath() . '/';
        } else {
            $validatedPrefix = '';
        }

        return $validatedPrefix;
    }

    /**
     * @return string Absolute path to the l10n directory
     */
    public static function getLabelsPath(): string
    {
        return Environment::getLabelsPath() . '/';
    }

    /**
     * converts an absolute or relative typo3 style (EXT:) file path
     *
     * @param string $file absolute file or a TYPO3 relative file (EXT:)
     * @param bool $absolute generate to relative(false) or absolute file
     * @return string converted file path
     *
     * @throws \Exception Conversion of file path failed
     */
    public static function transTypo3File($file, $absolute): string
    {
        $path = $file;
        if (!$absolute) {
            $fileLocation = self::checkFileLocation($path);
            if ($fileLocation === 'local') {
                $pathToRemove = Environment::getExtensionsPath() . '/';
            } elseif ($fileLocation === 'system') {
                $pathToRemove = Environment::getFrameworkBasePath() . '/';
            } elseif ($fileLocation === 'vendor') {
                $pathToRemove = Environment::getProjectPath() . '/vendor/';
                $path = SgLib::trimPath($pathToRemove, $path);
                // remove the vendor-name of the extension, too
                return 'EXT:' . substr($path, strpos($path, '/') + 1);
            } else {
                throw new \Exception('Can not convert absolute file "' . $file . '"');
            }

            $path = 'EXT:' . SgLib::trimPath($pathToRemove, $path);
        }

        return $path;
    }

    /**
     * converts an absolute or relative typo3 style (EXT:) file path
     *
     * @param string $file absolute file or a TYPO3 relative file (EXT:)
     * @return array converted file path
     * @throws \Exception Conversion of file path failed
     */
    public static function getExtNameFromOverrideFile($file): array
    {
        $absoluteWebPath = Environment::getProjectPath();
        $prefixedWithSettingsPath = str_replace($absoluteWebPath, '', $file);
        $alreadyExtPrefixedPath = str_replace(
            Environment::getPublicPath() . '/lfeditor/OverrideFiles/',
            '',
            $prefixedWithSettingsPath
        );
        $extName = explode('/', $alreadyExtPrefixedPath)[0];
        return [$extName, 'EXT:' . $alreadyExtPrefixedPath];
    }

    /**
     * generates portable file paths
     *
     * @param string $file file
     * @return string fixed file
     */
    public static function fixFilePath($file): string
    {
        return GeneralUtility::fixWindowsFilePath(str_replace('//', '/', $file));
    }

    /**
     * Adds configuration line to AdditionalConfiguration file.
     *
     * @param string $configLine line to be added.
     * @param string $additionalConfigurationFilePath
     */
    public static function writeLineToAdditionalConfiguration($configLine, $additionalConfigurationFilePath): void
    {
        SgLib::appendToPHPFile($additionalConfigurationFilePath, $configLine);
    }
}
