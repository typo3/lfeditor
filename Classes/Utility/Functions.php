<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\Lfeditor\Utility;

use Exception;
use SGalinski\Lfeditor\Service\FileBaseService;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * contains functions for the 'lfeditor' extension
 */
class Functions
{
    /**
     * Prepares the extension array.
     *
     * This function creates the surface of the select box and adds
     * some additional information to each entry.
     *
     * Structure of file array:
     * $fileArray[textHeader] = further arrays with extension paths
     *
     * @param array $fileArray see above
     * @return array prepared array
     */
    public static function prepareExtList($fileArray)
    {
        $myArray = [];
        foreach ($fileArray as $header => $extPaths) {
            if (!is_array($extPaths) || !count($extPaths)) {
                continue;
            }

            unset($prepArray);
            foreach ($extPaths as $extPath) {
                $prepArray[$extPath] = basename($extPath);
            }
            ksort($prepArray);
            /** @noinspection SlowArrayOperationsInLoopInspection */
            $myArray = array_merge($myArray, ['###extensionGroup###' . $header => $header], $prepArray);
        }
        return $myArray;
    }

    /**
     * searches extensions in a given path
     *
     * Modes for $state:
     * 0 - loaded and unloaded
     * 1 - only loaded
     * 2 - only unloaded
     *
     * @param string $path path
     * @param int $state optional: extension state to ignore (see above)
     * @param string $extIgnoreRegExp optional: directories to ignore (regular expression; pcre with slashes)
     * @param string $extWhitelistRegExp optional: keep only those directories (regular expression; pcre with slashes)
     * @return array result of the search
     * @throws Exception raised, if the given path cant be opened for reading
     */
    public static function searchExtensions($path, $state = 0, $extIgnoreRegExp = '', $extWhitelistRegExp = '')
    {
        $state = (int) $state;
        if (!@$fhd = opendir($path)) {
            throw new Exception('cant open "' . $path . '"');
        }

        $path = rtrim($path, '/');
        $extArray = [];
        while ($extDir = readdir($fhd)) {
            $extDirPath = $path . '/' . $extDir;

            // ignore all unless the file is a directory and no point dir
            if (!is_dir($extDirPath) || preg_match('/^\.{1,2}$/', $extDir)) {
                continue;
            }

            // check, if the directory/extension should be saved
            if (preg_match($extIgnoreRegExp, $extDir)) {
                continue;
            }

            // check, if the directory/extension should be saved
            if ($extWhitelistRegExp !== '' && !preg_match($extWhitelistRegExp, $extDir)) {
                continue;
            }

            // state filter
            if ($state) {
                $extState = (int) ExtensionManagementUtility::isLoaded($extDir);
                if (($extState && $state === 2) || (!$extState && $state === 1)) {
                    continue;
                }
            }

            $extArray[] = $extDirPath;
        }
        closedir($fhd);

        return $extArray;
    }

    /**
     * prepares a given language string for section output
     *
     * @param string $value language string
     * @return string prepared output in sections
     */
    public static function prepareSectionName($value)
    {
        return html_entity_decode(LocalizationUtility::translate($value, 'lfeditor'));
    }

    /**
     * checks and returns given languages or TYPO3 language list if the given content was empty
     *
     * @param array $languages optional: some language shortcuts
     * @return array language list
     */
    public static function buildLangArray($languages = null)
    {
        if (!is_array($languages) || !count($languages)) {
            return SgLib::getSystemLanguages();
        }
        return $languages;
    }

    /**
     * generates a general information array
     *
     * @param string $refLang reference language
     * @param array $languages language key array
     * @param FileBaseService $fileObj file object
     * @return array general information array
     * @see outputGeneral()
     */
    public static function genGeneralInfoArray($refLang, $languages, $fileObj)
    {
        $numTranslated = [];
        // reference language data information
        $localRefLangData = $fileObj->getLocalLangData($refLang);

        // generate needed data
        $infos = [];
        foreach ($languages as $langKey) {
            // get origin data and meta information
            $origin = $fileObj->getOriginLangData($langKey);
            $infos['default']['meta'] = $fileObj->getMetaData();

            // language data
            $localLangData = $fileObj->getLocalLangData($langKey);

            // detailed constants information
            $infos[$langKey]['numUntranslated'] =
                count(array_diff_key($localRefLangData, $localLangData));
            $infos[$langKey]['numUnknown'] =
                count(array_diff_key($localLangData, $localRefLangData));
            $infos[$langKey]['numTranslated'] =
                count(array_intersect_key($localLangData, $localRefLangData));

            // set origin
            try {
                $infos[$langKey]['origin'] = '[-]';
                if (!empty($origin)) {
                    $infos[$langKey]['origin'] = Typo3Lib::transTypo3File($origin, false);
                }
            } catch (Exception) {
                $pathSite = Environment::getPublicPath() . '/';
                $infos[$langKey]['origin'] = SgLib::trimPath($pathSite, $origin);
            }
        }

        // Sort by numTranslated DESC
        foreach ($infos as $key => $row) {
            $numTranslated[$key] = $row['numTranslated'];
        }
        array_multisort($numTranslated, SORT_DESC, $infos);

        return $infos;
    }
}
