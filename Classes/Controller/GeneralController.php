<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\Lfeditor\Controller;

use Psr\Http\Message\ResponseInterface;
use SGalinski\Lfeditor\Exceptions\LFException;
use SGalinski\Lfeditor\Utility\Functions;
use SGalinski\Lfeditor\Utility\SgLib;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * General controller. It contains extbase actions for general page.
 */
class GeneralController extends AbstractBackendController
{
    /** Code for normal split operation */
    public const NORMAL_SPLIT = 1;

    /** Code for merge operation */
    public const MERGE = 2;

    /**
     * Renders last-opened page. This action ai called upon opening LFEditor.
     *
     * @param bool $doStateRedirect
     */
    public function indexAction($doStateRedirect = true): ResponseInterface
    {
        if ($doStateRedirect) {
            $this->redirectToLastCalledControllerActionPair();
        }

        $this->resetLastCalledControllerActionPair();
        return $this->redirect('general', 'General');
    }

    /**
     * Opens general view.
     * It is called in 2 cases:
     * - on selection of general option in main menu,
     * - after redirection from action which must not change the view.
     *
     * @throws NoSuchCacheException
     */
    public function generalAction(): ResponseInterface
    {
        $docHeaderComponent = $this->moduleTemplate->getDocHeaderComponent();
        $pageUid = $this->request->getQueryParams()['id'] ?? 0;
        /** @var BackendUserAuthentication $backendUser */
        $backendUser = $GLOBALS['BE_USER'];
        // create docheader + buttons
        $pageInfo = BackendUtility::readPageAccess($pageUid, $backendUser->getPagePermsClause(1));
        if ($pageInfo === false) {
            $pageInfo = ['uid' => $pageUid];
        }

        $docHeaderComponent->setMetaInformation($pageInfo);
        try {
            $this->moduleTemplate->assign('controllerName', 'General');
            $this->docHeaderService->makeButtons($this->moduleTemplate->getDocHeaderComponent(), 'web_LfeditorGeneral');
            $this->prepareExtensionAndLangFileOptions();
            if (!$this->session->getDataByKey('languageFileSelection')) {
                throw new LFException('failure.select.noLangfile', 1);
            }

            $this->prepareGeneralViewMainSectionContent();
        } catch (LFException $e) {
            $this->addLFEFlashMessage($e);
        }

        $this->commonViewRenderingActionSettings();
        return $this->moduleTemplate->renderResponse('General');
    }

    /**
     * This action saves in session currently selected options from selection menus in general view.
     * It is called on change of selection of any select menu in general view.
     *
     * @param string $extensionSelection
     * @param string $languageFileSelection
     * @param string $extKey
     */
    public function changeSelectionAction(
        $extensionSelection = null,
        $languageFileSelection = null,
        $extKey = ''
    ): ResponseInterface {
        $this->saveSelectionsInSession(
            $extensionSelection,
            $languageFileSelection,
            null,
            null,
            null,
            null,
            null,
            null,
            $extKey
        );

        return $this->redirect('general');
    }

    /**
     * Saves the changes made in main section of general view.
     *
     * @param string $transformFile
     * @param int $splitFile
     * @throws NoSuchCacheException
     */
    public function generalSaveAction(
        $transformFile = null,
        $splitFile = null
    ): ResponseInterface {
        try {
            $metaArray = [
                'authorName' => '',
                'authorEmail' => '',
                'description' => '',
                'type' => '',
            ];
            $this->configurationService->execWrite([], $metaArray);
            // split or merge
            if ($transformFile === 'xlf') {
                $splitFile = self::NORMAL_SPLIT;
            }

            if (($splitFile == self::NORMAL_SPLIT || $splitFile == self::MERGE)) {
                $langModes = [];
                // set vars
                if ($splitFile != self::NORMAL_SPLIT && $splitFile != self::MERGE) {
                    $splitFile = 0;
                }

                $langKeys = Functions::buildLangArray();
                // generate langModes
                foreach ($langKeys as $langKey) {
                    if (!isset($langModes[$langKey])) {
                        $langModes[$langKey] = $splitFile;
                    }
                }

                // exec split or merge
                $this->configurationService->execSplitFile($langModes);
                // reinitialize file object
                $this->configurationService->initFileObject(
                    $this->session->getDataByKey('languageFileSelection'),
                    $this->session->getDataByKey('extensionSelection')
                );
            }

            if (!empty($transformFile)
                && $this->configurationService->getFileObj()->getVar('fileType') != $transformFile
            ) {
                $newFile = SgLib::setFileExtension(
                    $transformFile,
                    $this->configurationService->getFileObj()->getVar('relFile')
                );
                $this->configurationService->execTransform($transformFile, $newFile);
                $this->clearSelectOptionsCache('extensionAndLangFileOptions');
            }

            $this->addFlashMessage(
                LocalizationUtility::translate('lang.file.write.success', 'lfeditor')
            );
        } catch (LFException $e) {
            $this->addLFEFlashMessage($e);
        }

        return $this->redirect('general');
    }

    /**
     * Clears extensionAndLangFileOptions cache, and in that way refreshes list of language file options in select box.
     *
     * @throws NoSuchCacheException
     */
    public function refreshLanguageFileListAction(): ResponseInterface
    {
        $this->clearSelectOptionsCache('extensionAndLangFileOptions');
        return $this->redirect('general');
    }

    /**
     * Prepares main section content of general view.
     *
     * @throws LFException
     */
    protected function prepareGeneralViewMainSectionContent()
    {
        $this->configurationService->initFileObject(
            $this->session->getDataByKey('languageFileSelection'),
            $this->session->getDataByKey('extensionSelection')
        );
        $extConfig = $this->configurationService->getExtConfig();
        $referenceLanguageSelection = $extConfig['defaultLanguage'];
        $langArray = $this->configurationService->getLangArray($this->backendUser);
        $infoArray = Functions::genGeneralInfoArray(
            $referenceLanguageSelection,
            $langArray,
            $this->configurationService->getFileObj()
        );
        $langFileExtension = $this->configurationService->getFileObj()->getVar('fileType');
        $preselectMerge = $this->isOriginSameForAllLanguages($infoArray);

        $this->moduleTemplate->assign('infos', $infoArray);
        $this->moduleTemplate->assign('refLangNumTranslated', $infoArray[$referenceLanguageSelection]['numTranslated']);
        $this->moduleTemplate->assign('numTextAreaRows', $extConfig['numTextAreaRows']);
        $this->moduleTemplate->assign('langFileExtension', $langFileExtension);
        $this->moduleTemplate->assign('preselectMerge', $preselectMerge);
    }

    /**
     * Prepares parameters for redirection to viewTreeAction.
     *
     * @param string $language
     * @return ResponseInterface
     */
    public function goToEditFileAction($language): ResponseInterface
    {
        $this->session->setDataByKey('languageSelection', $language);
        return $this->redirect('editFile', 'EditFile');
    }

    /**
     * Switches between override mode and normal mode.
     *
     * @param string $editingMode 'extension', 'l10n', 'override'.
     * @return ResponseInterface
     */
    public function switchEditingModeAction($editingMode = 'extension'): ResponseInterface
    {
        $this->session->setDataByKey('editingMode', $editingMode);
        return $this->indexAction();
    }

    /**
     * Checks do all language translations originate from same file.
     *
     * @param array $infoArray
     * @return bool
     */
    private function isOriginSameForAllLanguages(array $infoArray)
    {
        foreach ($infoArray as $langInfo) {
            if ($infoArray['default']['origin'] !== $langInfo['origin']) {
                return false;
            }
        }

        return true;
    }
}
