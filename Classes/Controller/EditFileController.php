<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\Lfeditor\Controller;

use Psr\Http\Message\ResponseInterface;
use SGalinski\Lfeditor\Exceptions\LFException;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * EditFile controller. It contains extbase actions of EditFile page.
 */
class EditFileController extends AbstractBackendController
{
    /**
     * Opens editFile view.
     * It is called in 2 cases:
     * - on selection of editFile option in main menu,
     * - after redirection from action which must not change the view.
     *
     * @param int $buttonType :
     *                    -1 - cancel,
     *                     0 - no button clicked,
     *                     1 - back,
     *                     2 - next,
     *                     3 - save.
     * @throws NoSuchCacheException
     */
    public function editFileAction(int $buttonType = 0): ResponseInterface
    {
        $docHeaderComponent = $this->moduleTemplate->getDocHeaderComponent();
        $pageUid = $this->request->getQueryParams()['id'] ?? 0;
        /** @var BackendUserAuthentication $backendUser */
        $backendUser = $GLOBALS['BE_USER'];
        // create docheader + buttons
        $pageInfo = BackendUtility::readPageAccess($pageUid, $backendUser->getPagePermsClause(1));
        if ($pageInfo === false) {
            $pageInfo = ['uid' => $pageUid];
        }

        $docHeaderComponent->setMetaInformation($pageInfo);
        $this->docHeaderService->makeButtons($this->moduleTemplate->getDocHeaderComponent(), 'web_LfeditorGeneral');
        try {
            $this->moduleTemplate->assign('controllerName', 'EditFile');
            $this->prepareExtensionAndLangFileOptions();
            $this->configurationService->initFileObject(
                $this->session->getDataByKey('languageFileSelection'),
                $this->session->getDataByKey('extensionSelection')
            );
            $langData = $this->configurationService->getFileObj()->getLocalLangData();
            $languageOptions = $this->configurationService->menuLangList($langData, '', $this->backendUser);
            $this->assignViewWidthMenuVariables('language', $languageOptions);
            $referenceLanguageOptions = $this->configurationService->menuLangList(
                $langData,
                LocalizationUtility::translate('select.nothing', 'lfeditor'),
                $this->backendUser
            );
            $this->assignViewWidthMenuVariables('referenceLanguage', $referenceLanguageOptions);
            $bottomReferenceLanguageOptions = $this->configurationService->menuLangList(
                $langData,
                '',
                $this->backendUser,
                true
            );
            $this->assignViewWidthMenuVariables('bottomReferenceLanguage', $bottomReferenceLanguageOptions);
            $constantTypeOptions = $this->getConstantTypeOptions();
            $this->assignViewWidthMenuVariables('constantType', $constantTypeOptions);
            $extConfig = $this->configurationService->getExtConfig();
            $this->assignViewWidthMenuVariables('numSiteConsts', $extConfig['numSiteConstsOptions']);
            $this->prepareEditFileViewMainSectionContent($langData, $buttonType);
        } catch (LFException $e) {
            $this->addLFEFlashMessage($e);
        }

        $this->commonViewRenderingActionSettings();
        return $this->moduleTemplate->renderResponse('EditFile');
    }

    /**
     * This action saves in session currently selected options from selection menus in editFile view.
     * It is called on change of selection of any select menu in editFile view.
     *
     * @param string $extensionSelection
     * @param string $languageFileSelection
     * @param string $languageSelection
     * @param string $referenceLanguageSelection
     * @param string $constantTypeSelection
     * @param string $bottomReferenceLanguageSelection
     * @param string $numSiteConstsSelection
     * @param string $extKey
     */
    public function changeSelectionAction(
        $extensionSelection = null,
        $languageFileSelection = null,
        $languageSelection = null,
        $referenceLanguageSelection = null,
        $constantTypeSelection = null,
        $bottomReferenceLanguageSelection = null,
        $numSiteConstsSelection = null,
        $extKey = ''
    ): ResponseInterface {
        $this->saveSelectionsInSession(
            $extensionSelection,
            $languageFileSelection,
            $referenceLanguageSelection,
            null,
            $languageSelection,
            $constantTypeSelection,
            $bottomReferenceLanguageSelection,
            $numSiteConstsSelection,
            $extKey
        );

        return $this->redirect('editFile', null, null, ['buttonType' => 0]);
    }

    /**
     * Clears extensionAndLangFileOptions cache, and in that way refreshes list of language file options in select box.
     */
    public function refreshLanguageFileListAction(): ResponseInterface
    {
        $this->clearSelectOptionsCache('extensionAndLangFileOptions');
        return $this->redirect('editFile', null, null, ['buttonType' => 0]);
    }

    /**
     * Prepares main section content of editFile view.
     * Structure of the content:
     * $constValues[$constant]['edit'],
     * $constValues[$constant]['pattern'],
     * $constValues[$constant][$extConfig['defaultLanguage']].
     *
     * @param array $langData
     * @param int $buttonType :
     *                    -1 - cancel,
     *                     0 - no button clicked,
     *                     1 - back,
     *                     2 - next,
     *                     3 - save.
     * @throws LFException
     */
    protected function prepareEditFileViewMainSectionContent(array $langData, int $buttonType)
    {
        $extConfig = $this->configurationService->getExtConfig();
        $numConstantsPerPage = $this->session->getDataByKey('numSiteConstsSelection');

        $langList = $this->session->getDataByKey('languageSelection');
        $patternList = $this->session->getDataByKey('referenceLanguageSelection');
        $constTypeList = $this->session->getDataByKey('constantTypeSelection');
        $parallelEdit = $patternList !== '###default###' && $patternList !== $langList;

        $langEdit = (array_key_exists($langList, $langData) && is_array(
            $langData[$langList]
        )) ? $langData[$langList] : [];
        $langPattern = (array_key_exists($patternList, $langData) && is_array(
            $langData[$patternList]
        )) ? $langData[$patternList] : [];
        $bottomReferenceLanguageSelection = $this->session->getDataByKey('bottomReferenceLanguageSelection');
        $bottomReferenceLanguage = is_array($langData[$bottomReferenceLanguageSelection])
            ? $langData[$bottomReferenceLanguageSelection] : [];
        if (empty($bottomReferenceLanguage)) {
            $bottomReferenceLanguage = is_array($langData['default']) ? $langData['default'] : [];
        }

        $langDataSessionContinued = $buttonType !== 3;
        if ($buttonType === 0) {
            $this->session->setDataByKey('sessionLangDataConstantsIterator', 0);
            $this->session->setDataByKey('numberLastPageConstants', 0);
        }
        $sessionLangDataConstantsIterator = $this->session->getDataByKey('sessionLangDataConstantsIterator');
        $numberLastPageConstants = $this->session->getDataByKey('numberLastPageConstants');

        // new translation
        if (!$langDataSessionContinued || $buttonType <= 0) {
            // adjust number of session constants
            if ($constTypeList === 'untranslated' || $constTypeList === 'translated' ||
                $constTypeList === 'unknown' || $buttonType <= 0
            ) {
                $sessionLangDataConstantsIterator = 0;
            } elseif (!$langDataSessionContinued) { // session written to file
                $sessionLangDataConstantsIterator -= $numberLastPageConstants;
            }

            // delete old data in session
            $this->session->setDataByKey('langfileEditNewLangData', null);
            $this->session->setDataByKey('langfileEditConstantsList', null);

            // get language data
            if ($constTypeList === 'untranslated') {
                $myLangData = array_diff_key($bottomReferenceLanguage, $langEdit);
            } elseif ($constTypeList === 'unknown') {
                $myLangData = array_diff_key($langEdit, $bottomReferenceLanguage);
            } elseif ($constTypeList === 'translated') {
                $myLangData = array_intersect_key($bottomReferenceLanguage, $langEdit);
            } else {
                $myLangData = $bottomReferenceLanguage;
            }
            $this->session->setDataByKey('langfileEditConstantsList', array_keys($myLangData));
        } elseif ($buttonType === 1) { // back button
            $sessionLangDataConstantsIterator -= ($numConstantsPerPage + $numberLastPageConstants);
        }

        /** @var array $langData */
        /** @noinspection CallableParameterUseCaseInTypeContextInspection */
        $langData = $this->session->getDataByKey('langfileEditConstantsList');
        $numConsts = is_countable($langData) ? count($langData) : 0;
        if (!(is_countable($langData) ? count($langData) : 0)) {
            throw new LFException('failure.select.emptyLangDataArray', 1);
        }

        /** @var array $langfileEditNewLangData */
        $langfileEditNewLangData = $this->session->getDataByKey('langfileEditNewLangData');
        if (!is_array($langfileEditNewLangData)) {
            $langfileEditNewLangData = [];
        }

        // prepare constant list for this page
        $numberLastPageConstants = 0;
        $constValues = [];
        do {
            // check number of session constants
            if ($sessionLangDataConstantsIterator >= $numConsts) {
                break;
            }
            ++$numberLastPageConstants;

            // set constant value (maybe already changed in this session)
            $constant = $langData[$sessionLangDataConstantsIterator];
            $editLangVal = $langEdit[$constant] ?? '';
            if (!isset($langfileEditNewLangData[$langList][$constant])) {
                $langfileEditNewLangData[$langList][$constant] = $editLangVal;
            } else {
                $editLangVal = $langfileEditNewLangData[$langList][$constant];
            }

            // set constant value (maybe already changed in this session)
            $editPatternVal = $langPattern[$constant] ?? '';
            if (!isset($langfileEditNewLangData[$patternList][$constant])) {
                $langfileEditNewLangData[$patternList][$constant] = $editPatternVal;
            } else {
                $editPatternVal =
                    $langfileEditNewLangData[$patternList][$constant];
            }

            // save information about the constant
            $constValues[$constant]['edit'] = $editLangVal;
            $constValues[$constant]['pattern'] = $editPatternVal;
            $constValues[$constant]['default'] = $bottomReferenceLanguage[$constant];
        } while (++$sessionLangDataConstantsIterator % $numConstantsPerPage);

        $this->session->setDataByKey('langfileEditNewLangData', $langfileEditNewLangData);
        $this->session->setDataByKey('sessionLangDataConstantsIterator', $sessionLangDataConstantsIterator);
        $this->session->setDataByKey('numberLastPageConstants', $numberLastPageConstants);

        $this->moduleTemplate->assign('numTextAreaRows', $extConfig['numTextAreaRows']);
        $this->moduleTemplate->assign('defaultLanguage', $extConfig['defaultLanguage']);
        $this->moduleTemplate->assign('parallelEdit', $parallelEdit);
        $this->moduleTemplate->assign('displayBackButton', $sessionLangDataConstantsIterator > $numConstantsPerPage);
        $this->moduleTemplate->assign('displayNextButton', $sessionLangDataConstantsIterator < $numConsts);

        $this->moduleTemplate->assign('constValues', $constValues);
        $this->moduleTemplate->assign('curConsts', $sessionLangDataConstantsIterator);
        $this->moduleTemplate->assign('totalConsts', $numConsts);
        $this->moduleTemplate->assign('numSiteConstsSelection', $numConstantsPerPage);
    }

    /**
     * Used for the constant type selector
     *
     * @return array
     */
    private function getConstantTypeOptions(): array
    {
        $constantTypeOptions = [];
        $constantTypeOptions['all'] = LocalizationUtility::translate('const.type.all', 'lfeditor');
        $constantTypeOptions['translated'] = LocalizationUtility::translate('const.type.translated', 'lfeditor');
        $constantTypeOptions['unknown'] = LocalizationUtility::translate('const.type.unknown', 'lfeditor');
        $constantTypeOptions['untranslated'] = LocalizationUtility::translate('const.type.untranslated', 'lfeditor');
        return $constantTypeOptions;
    }

    /**
     * Saves the changes made in main section of editFile view.
     *
     * @param int $buttonType
     * @param array $editFileTextArea editFileTextArea[{languageSelection}][{constKey}]
     * @return ResponseInterface
     * @throws NoSuchCacheException
     */
    public function editFileSaveAction(int $buttonType, array $editFileTextArea): ResponseInterface
    {
        try {
            $languageSelection = $this->session->getDataByKey('languageSelection');
            $referenceLanguageSelection = $this->session->getDataByKey('referenceLanguageSelection');
            $langDataSessionContinued = $buttonType !== 3;

            $langfileEditNewLangData = $this->session->getDataByKey('langfileEditNewLangData');
            $langfileEditNewLangData[$languageSelection] =
                array_merge(
                    $langfileEditNewLangData[$languageSelection] ?? [],
                    $editFileTextArea[$languageSelection] ?? []
                );

            // parallel edit mode?
            if ($referenceLanguageSelection !== '###default###' && $referenceLanguageSelection !== $languageSelection) {
                $langfileEditNewLangData[$referenceLanguageSelection] =
                    array_merge(
                        $langfileEditNewLangData[$referenceLanguageSelection],
                        $editFileTextArea[$referenceLanguageSelection] ?? []
                    );
            }
            $this->session->setDataByKey('langfileEditNewLangData', $langfileEditNewLangData);

            // write if no session continued
            if (!$langDataSessionContinued) {
                // Making array of languages that were changed, so only that language files will be edited.
                $editedLanguages = [$languageSelection];
                if ($languageSelection !== $referenceLanguageSelection) {
                    $editedLanguages[] = $referenceLanguageSelection;
                }

                $this->configurationService->execWrite($langfileEditNewLangData, [], false, $editedLanguages);
                $this->addFlashMessage(LocalizationUtility::translate('lang.file.write.success', 'lfeditor'));
            }
        } catch (LFException $e) {
            $this->addLFEFlashMessage($e);
        }

        return $this->redirect('editFile', null, null, ['buttonType' => $buttonType]);
    }
}
