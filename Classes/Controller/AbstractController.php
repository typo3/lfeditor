<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\Lfeditor\Controller;

use SGalinski\Lfeditor\Exceptions\DirectoryAccessRightsException;
use SGalinski\Lfeditor\Exceptions\LFException;
use SGalinski\Lfeditor\Service\ConfigurationService;
use SGalinski\Lfeditor\Session\PhpSession;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Type\ContextualFeedbackSeverity;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Abstract Controller
 */
abstract class AbstractController extends ActionController
{
    /**
     * @var PhpSession
     */
    protected PhpSession $session;

    /**
     * @var ConfigurationService
     */
    protected ConfigurationService $configurationService;

    /**
     * Initializes the actions.
     * - Initializes the session object.
     * - Fetches configuration.
     *
     * @param PhpSession $session
     * @param ConfigurationService $configurationService
     * @throws DirectoryAccessRightsException
     */
    public function __construct(
        PhpSession $session,
        ConfigurationService $configurationService
    ) {
        $this->session = $session;
        $this->session->setSessionKey('tx_lfeditor_sessionVariables');

        $this->configurationService = $configurationService;
        $this->configurationService->prepareConfig();
    }

    /**
     * Saves in session currently selected values of select tags.
     *
     * @param string|null $extensionSelection
     * @param string|null $languageFileSelection
     * @param string|null $referenceLanguageSelection
     * @param string|null $constantSelection
     * @param string|null $languageSelection
     * @param string|null $constantTypeSelection
     * @param string|null $bottomReferenceLanguageSelection
     * @param string|null $numSiteConstsSelection
     * @param string|null $extkey
     */
    protected function saveSelectionsInSession(
        string $extensionSelection = null,
        string $languageFileSelection = null,
        string $referenceLanguageSelection = null,
        string $constantSelection = null,
        string $languageSelection = null,
        string $constantTypeSelection = null,
        string $bottomReferenceLanguageSelection = null,
        string $numSiteConstsSelection = null,
        string $extkey = null
    ): void {
        /* Extension/language file select box can't be unselected.
        Only situation when $extensionSelection === NULL is when the form is submitted by
        selection change of some other box. That is because <f:be.menus.actionMenu> with 'optgroup' tags is used */
        if ($extensionSelection) {
            $this->session->setDataByKey('extensionSelection', $extensionSelection);
        }
        if ($languageFileSelection) {
            $this->session->setDataByKey('languageFileSelection', $languageFileSelection);
        }
        if ($referenceLanguageSelection) {
            $this->session->setDataByKey('referenceLanguageSelection', $referenceLanguageSelection);
        }
        if ($constantSelection) {
            $this->session->setDataByKey('constantSelection', $constantSelection);
        }
        if ($languageSelection) {
            $this->session->setDataByKey('languageSelection', $languageSelection);
        }
        if ($constantTypeSelection) {
            $this->session->setDataByKey('constantTypeSelection', $constantTypeSelection);
        }
        if ($bottomReferenceLanguageSelection) {
            $this->session->setDataByKey('bottomReferenceLanguageSelection', $bottomReferenceLanguageSelection);
        }
        if ($numSiteConstsSelection) {
            $this->session->setDataByKey('numSiteConstsSelection', $numSiteConstsSelection);
        }
        if ($extkey) {
            $this->session->setDataByKey('extkey', $extkey);
        }
    }

    /**
     * Assigns view width menu options and default menu selection which is fetched from session.
     *
     * Example: menuName 'extension' will produce view keys 'extensionOptions' and 'extensionSelection'
     *
     * @param string $menuName Name of the menu, which will be used as prefix of view keys for menu options and menu selection.
     * @param array $options menu options to be assigned to view
     */
    protected function assignViewWidthMenuVariables(string $menuName, array $options): void
    {
        $this->moduleTemplate->assign($menuName . 'Options', $options);

        $selection = $this->checkMenuSelection($menuName, $options);
        $this->moduleTemplate->assign($menuName . 'Selection', $selection);
    }

    /**
     * Checks does selection exist in session or among menu options and if it does not,
     * first option becomes selected.
     *
     * Example: menuName 'extension' will produce view keys 'extensionOptions' and 'extensionSelection'
     *
     * @param string $menuName Name of the menu, which will be used as prefix of view keys for menu options and menu selection.
     * @param array $options menu options to be checked upon.
     */
    protected function checkMenuSelection(string $menuName, array $options): ?string
    {
        $selection = $this->session->getDataByKey($menuName . 'Selection');
        if (!\array_key_exists($selection, $options)) {
            $selection = null;
        }

        if ($selection === null && !empty($options)) {
            $selection = array_key_first($options);
            $this->session->setDataByKey($menuName . 'Selection', $selection);
            return $selection;
        }

        return $selection;
    }

    /**
     * Sets FlashMessage from LFException.
     *
     * @param LFException $lFException
     */
    public function addLFEFlashMessage(LFException $lFException): void
    {
        if ($lFException->getCode() === 0) {
            $this->addFlashMessage(
                $lFException->getMessage(),
                LocalizationUtility::translate('failure.failure', 'lfeditor'),
                ContextualFeedbackSeverity::ERROR
            );
        } elseif ($lFException->getCode() === 1) {
            $this->addFlashMessage(
                $lFException->getMessage(),
                '',
                ContextualFeedbackSeverity::NOTICE
            );
        }
    }

    /**
     * Prepares language file select options for each extension and sets combined data in view.
     *
     * @throws NoSuchCacheException
     * @throws LFException
     */
    protected function prepareExtensionAndLangFileOptions(): void
    {
        $extensions = $this->getExtensions();
        $this->checkExtensionAndLangFileSelection($extensions);
        $extensionSelection = $this->session->getDataByKey('extensionSelection');

        $this->moduleTemplate->assign('extensions', $extensions);
        $this->moduleTemplate->assign('extensionSelection', $extensionSelection);
        $this->moduleTemplate->assign('extensionLabel', $extensions[$extensionSelection]['extLabel'] ?? '');
        $this->moduleTemplate->assign('languageFileSelection', $this->session->getDataByKey('languageFileSelection'));
    }

    /**
     * Checks do extensionSelection and languageFileSelection exist in session or among menu options and if it does not,
     * first language file and belonging extension become selected and saved to session.
     *
     * @param array $extensions
     */
    protected function checkExtensionAndLangFileSelection(array $extensions): void
    {
        $extensionSelection = $this->session->getDataByKey('extensionSelection');
        $languageFileSelection = $this->session->getDataByKey('languageFileSelection');

        $selectFirstLanguageFileAndBelongingExtension = !$extensionSelection || !$languageFileSelection ||
            !(array_key_exists($extensionSelection, $extensions) && array_key_exists(
                $languageFileSelection,
                $extensions[$extensionSelection]['languageFileOptions']
            ));
        if ($selectFirstLanguageFileAndBelongingExtension) {
            foreach ($extensions as $extAddress => $extension) {
                if (empty($extension['languageFileOptions'])) {
                    continue;
                }
                $languageFileSelection = array_key_first($extension['languageFileOptions']);
                $this->session->setDataByKey('extkey', $extension['extKey']);
                $this->session->setDataByKey('languageFileSelection', $languageFileSelection);
                $this->session->setDataByKey('extensionSelection', $extAddress);
                break;
            }
        }
    }

    /**
     * Clears cache used for storing select options.
     * If $identifier is set, it clears only that entry in cache,
     * otherwise it clears whole select options cache.
     *
     * @param string|null $identifier
     * @throws NoSuchCacheException
     */
    protected function clearSelectOptionsCache(string $identifier = null): void
    {
        /** @var CacheManager $cacheManager */
        $cacheManager = GeneralUtility::makeInstance(CacheManager::class);
        if ($identifier !== null) {
            $cacheManager->getCache('lfeditor_select_options_cache')->set($identifier, null);
        } else {
            $cacheManager->getCache('lfeditor_select_options_cache')->flush();
        }
    }

    /**
     * @return array
     * @throws LFException
     * @throws NoSuchCacheException
     */
    protected function getExtensions(): array
    {
        /** @var CacheManager $cacheManager */
        $cacheManager = GeneralUtility::makeInstance(CacheManager::class);
        $cache = $cacheManager->getCache('lfeditor_select_options_cache');
        $extensions = $cache->get('extensionAndLangFileOptions');
        if (!is_array($extensions)) {
            $extensions = [];
        }

        if (!count($extensions)) {
            $extensions = [];
            $extensionOptions = $this->configurationService->menuExtList();
            $extensionGroupCount = 0;
            foreach ($extensionOptions as $extAddress => $extLabel) {
                $extension['extKey'] = $extLabel;
                $isExtensionGroupStart = false;
                if (str_starts_with($extAddress, '###extensionGroup###')) {
                    $extension['extLabel'] = $extLabel;
                    $isExtensionGroupStart = true;
                } else {
                    $extKey = str_replace('-', '_', basename($extLabel));
                    if (str_starts_with($extKey, 'cms_')) {
                        $extKey = str_replace('cms_', '', $extKey);
                    }

                    ((int) ExtensionManagementUtility::isLoaded($extKey)) ?
                        $state = LocalizationUtility::translate('ext.loaded', 'lfeditor') :
                        $state = LocalizationUtility::translate('ext.notLoaded', 'lfeditor');
                    $extension['extLabel'] = $extLabel . ' [' . $state . ']';
                }

                $extension['languageFileOptions'] = [];
                try {
                    if (!$isExtensionGroupStart) {
                        $extension['languageFileOptions'] = $this->configurationService->menuLangFileList($extAddress);
                        if (empty($extension['languageFileOptions'])) {
                            continue;
                        }
                    } elseif (++$extensionGroupCount > 1) {
                        $extensions[$extAddress . 'EmptySpaceBefore'] = [
                            'extLabel' => '',
                            'languageFileOptions' => [],
                        ];
                    }
                } catch (LFException) {
                    continue;
                }

                $extensions[$extAddress] = $extension;
                if ($isExtensionGroupStart) {
                    $extensions[$extAddress . 'DelimiterAfter'] = [
                        'extLabel' => '======',
                        'languageFileOptions' => [],
                    ];
                }
            }

            $cache->set('extensionAndLangFileOptions', $extensions);
        }

        return $extensions;
    }
}
