<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\Lfeditor\Controller;

use SGalinski\Lfeditor\Exceptions\DirectoryAccessRightsException;
use SGalinski\Lfeditor\Service\DocHeaderService;
use TYPO3\CMS\Backend\Template\ModuleTemplate;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Http\ApplicationType;
use TYPO3\CMS\Extbase\Http\ForwardResponse;

/**
 * Abstract Backend Controller
 */
abstract class AbstractBackendController extends AbstractController
{
    /**
     * @var BackendUserAuthentication
     */
    protected $backendUser;

    /**
     * @var ModuleTemplate
     */
    protected $moduleTemplate;

    /**
     * @var ModuleTemplateFactory
     */
    protected $moduleTemplateFactory;

    protected DocHeaderService $docHeaderService;

    public function injectDocHeaderSerivice(DocHeaderService $docHeaderService)
    {
        $this->docHeaderService = $docHeaderService;
    }

    public function injectModuleTemplateFactory(ModuleTemplateFactory $moduleTemplateFactory)
    {
        $this->moduleTemplateFactory = $moduleTemplateFactory;
    }

    /**
     * Initializes any action
     *
     * @throws DirectoryAccessRightsException
     */
    public function initializeAction(): void
    {
        parent::initializeAction();
        $this->moduleTemplate = $this->moduleTemplateFactory->create($this->request);
        if (ApplicationType::fromRequest($GLOBALS['TYPO3_REQUEST'])->isBackend()) {
            $this->backendUser = $GLOBALS['BE_USER'];
        }

        $editingMode = $this->session->getDataByKey('editingMode');
        $availableEditingModes = $this->configurationService->getAvailableEditingModes();
        if ($this->backendUser->isAdmin()) {
            if (!\array_key_exists($editingMode, $availableEditingModes)) {
                $firstAvailableEditMode = \key($availableEditingModes);
                $this->session->setDataByKey('editingMode', $firstAvailableEditMode);
            }
            $canChangeEditingModes = \count($availableEditingModes) > 0;
        } else {
            $canChangeEditingModes = \count($availableEditingModes) > 0
                && $this->backendUser->user['lfeditor_change_editing_modes'] !== 0;
            if (!$canChangeEditingModes || !\array_key_exists($editingMode, $availableEditingModes)) {
                $lastAvailableEditMode = array_key_last($availableEditingModes);
                $this->session->setDataByKey('editingMode', $lastAvailableEditMode);
            }
        }

        $this->session->setDataByKey('defaultLanguagePermission', $this->backendUser->checkLanguageAccess(0));
        $this->session->setDataByKey('canChangeEditingModes', $canChangeEditingModes);
    }

    /**
     * Saves the last called controller/action pair into the backend user
     * configuration if available
     *
     * @param bool $saveWithRedirectPair
     */
    protected function setLastCalledControllerActionPair($saveWithRedirectPair = true)
    {
        if (!$this->backendUser) {
            return;
        }

        $extensionKey = $this->request->getControllerExtensionKey();
        $pair = [
            'action' => $this->request->getControllerActionName(),
            'controller' => $this->request->getControllerName(),
        ];
        $this->backendUser->uc[$extensionKey . 'State']['LastActionControllerPair'] = $pair;
        if ($saveWithRedirectPair) {
            $this->backendUser->uc[$extensionKey . 'State']['LastActionControllerPairForRedirect'] = $pair;
        }

        $this->backendUser->writeUC();
    }

    /**
     * Resets the last called controller/action pair combination from the
     * backend user session
     */
    protected function resetLastCalledControllerActionPair()
    {
        if (!$this->backendUser) {
            return;
        }

        $extensionKey = $this->request->getControllerExtensionKey();
        $this->backendUser->uc[$extensionKey . 'State']['LastActionControllerPair'] = [];
        $this->backendUser->uc[$extensionKey . 'State']['LastActionControllerPairForRedirect'] = [];
        $this->backendUser->writeUC();

        $this->moduleTemplate->assign('lastCalledControllerActionPair', null);
    }

    /**
     * Returns the last called controller/action pair from the backend user session
     *
     * @return array
     */
    protected function getLastCalledControllerActionPair()
    {
        if (!$this->backendUser) {
            return [];
        }

        $extensionKey = $this->request->getControllerExtensionKey();
        $state = $this->backendUser->uc[$extensionKey . 'State']['LastActionControllerPair'];
        return !\is_array($state) ? [] : $state;
    }

    /**
     * Returns the last called controller/action pair from the backend user session
     *
     * @return array
     */
    protected function getLastCalledControllerActionPairForRedirect()
    {
        if (!$this->backendUser) {
            return [];
        }

        $extensionKey = $this->request->getControllerExtensionKey();
        return $this->backendUser->uc[$extensionKey . 'State']['LastActionControllerPairForRedirect'] ?? [];
    }

    /**
     * Redirects to the last called controller/action pair saved inside the
     * backend user session
     *
     * @return ForwardResponse|null
     */
    protected function redirectToLastCalledControllerActionPair(): ?ForwardResponse
    {
        $state = $this->getLastCalledControllerActionPairForRedirect();
        if (\count($state) && \trim($state['action']) !== '' && \trim($state['controller']) !== '') {
            $currentAction = $this->request->getControllerActionName();
            $currentController = $this->request->getControllerName();
            if (!($currentController === $state['controller'] && $currentAction === $state['action'])) {
                $extensionName = $this->request->getControllerExtensionName();
                $moduleSignature = $this->request->getPluginName();
                $extensionConfig = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['extbase']['extensions'][$extensionName] ?? [];
                $availableControllers = $extensionConfig['modules'][$moduleSignature]['controllers'] ?? [];
                $controllerExists = isset($availableControllers[$state['controller']]);
                if ($controllerExists) {
                    $actionExists = \in_array(
                        $state['action'],
                        $availableControllers[$state['controller']]['actions'],
                        true
                    );
                    if ($actionExists) {
                        return (new ForwardResponse($state['action']))->withControllerName(
                            $state['controller']
                        );
                    }
                }
            }
        }

        return null;
    }

    /**
     * Sets last called controller-action pair and assigns common view variables.
     * This function should be called at the end of actions which render view
     * (and does not do redirection or forwarding at the end)
     */
    protected function commonViewRenderingActionSettings()
    {
        $this->setLastCalledControllerActionPair();
        $this->moduleTemplate->assign('editingMode', $this->session->getDataByKey('editingMode'));
        $this->moduleTemplate->assign('editingModeOptions', $this->configurationService->getAvailableEditingModes());
        $this->moduleTemplate->assign('controller', $this->request->getControllerName());
        $this->moduleTemplate->assign('action', $this->request->getControllerActionName());
        $this->moduleTemplate->assign('adminUser', $this->backendUser->isAdmin());
        $this->moduleTemplate->assign(
            'defaultLanguagePermission',
            $this->session->getDataByKey('defaultLanguagePermission')
        );
        $this->moduleTemplate->assign('canChangeEditingModes', $this->session->getDataByKey('canChangeEditingModes'));
    }
}
