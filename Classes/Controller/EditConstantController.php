<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\Lfeditor\Controller;

use Psr\Http\Message\ResponseInterface;
use SGalinski\Lfeditor\Exceptions\LFException;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * EditConstant controller. It contains extbase actions for EditConstant page.
 */
class EditConstantController extends AbstractBackendController
{
    /**
     * Opens editConstant view.
     * It is called in 2 cases:
     * - on selection of editConstant option in main menu,
     * - after redirection from action which must not change the view.
     *
     * @return ResponseInterface
     * @throws NoSuchCacheException
     */
    public function editConstantAction(): ResponseInterface
    {
        $docHeaderComponent = $this->moduleTemplate->getDocHeaderComponent();
        $pageUid = $this->request->getQueryParams()['id'] ?? 0;
        /** @var BackendUserAuthentication $backendUser */
        $backendUser = $GLOBALS['BE_USER'];
        // create docheader + buttons
        $pageInfo = BackendUtility::readPageAccess($pageUid, $backendUser->getPagePermsClause(1));
        if ($pageInfo === false) {
            $pageInfo = ['uid' => $pageUid];
        }

        $docHeaderComponent->setMetaInformation($pageInfo);
        $this->docHeaderService->makeButtons($this->moduleTemplate->getDocHeaderComponent(), 'web_LfeditorGeneral');
        try {
            $this->moduleTemplate->assign('controllerName', 'EditConstant');
            $this->prepareExtensionAndLangFileOptions();
            $this->configurationService->initFileObject(
                $this->session->getDataByKey('languageFileSelection'),
                $this->session->getDataByKey('extensionSelection')
            );
            $langData = $this->configurationService->getFileObj()->getLocalLangData();
            $constantOptions = $this->configurationService->menuConstList(
                $langData,
                LocalizationUtility::translate('select.nothing', 'lfeditor')
            );
            $this->assignViewWidthMenuVariables('constant', $constantOptions);
            $this->prepareEditConstantViewMainSectionContent($langData);
        } catch (LFException $e) {
            $this->addLFEFlashMessage($e);
        }

        $this->commonViewRenderingActionSettings();
        return $this->moduleTemplate->renderResponse('EditConstant');
    }

    /**
     * This action saves in session currently selected options from selection menus in editConstant view.
     * It is called on change of selection of any select menu in editConstant view.
     *
     * @param string $extensionSelection
     * @param string $languageFileSelection
     * @param string $constantSelection
     * @param string $extKey
     */
    public function changeSelectionAction(
        $extensionSelection = null,
        $languageFileSelection = null,
        $constantSelection = null,
        $extKey = ''
    ): ResponseInterface {
        $this->saveSelectionsInSession(
            $extensionSelection,
            $languageFileSelection,
            null,
            $constantSelection,
            null,
            null,
            null,
            null,
            $extKey
        );

        return $this->redirect('editConstant');
    }

    /**
     * Saves the changes made in main section of editConstant view.
     *
     * @param array $editConstTextArea
     * @return ResponseInterface
     * @throws NoSuchCacheException
     */
    public function editConstantSaveAction(array $editConstTextArea): ResponseInterface
    {
        try {
            $this->configurationService->execWrite($editConstTextArea);

            $this->addFlashMessage(
                LocalizationUtility::translate('lang.file.write.success', 'lfeditor')
            );
        } catch (LFException $e) {
            $this->addLFEFlashMessage($e);
        }

        return $this->redirect('editConstant');
    }

    /**
     * Clears extensionAndLangFileOptions cache, and in that way refreshes list of language file options in select box.
     *
     * @return ResponseInterface
     * @throws NoSuchCacheException
     */
    public function refreshLanguageFileListAction(): ResponseInterface
    {
        $this->clearSelectOptionsCache('extensionAndLangFileOptions');
        return $this->redirect('editConstant');
    }

    /**
     * Prepares main section content of editConstant view.
     *
     * @param array|null $langData
     * @throws LFException
     */
    protected function prepareEditConstantViewMainSectionContent(array $langData = null)
    {
        $extConfig = $this->configurationService->getExtConfig();
        $langArray = $this->configurationService->getLangArray($this->backendUser);

        $constantSelection = $this->session->getDataByKey('constantSelection');
        if (empty($constantSelection) || $constantSelection === '###default###') {
            throw new LFException('failure.select.noConst', 1);
        }

        $languages = [];
        foreach ($langArray as $lang) {
            if (array_key_exists($lang, $langData) && array_key_exists($constantSelection, $langData[$lang])) {
                $languages[$lang] = $langData[$lang][$constantSelection];
            } else {
                $languages[$lang] = '';
            }
        }

        $this->moduleTemplate->assign('constantSelection', $constantSelection);
        $this->moduleTemplate->assign('languages', $languages);
        $this->moduleTemplate->assign('numTextAreaRows', $extConfig['numTextAreaRows']);
        $this->moduleTemplate->assign('defaultLanguage', $extConfig['defaultLanguage']);
    }

    /**
     * Sets chosen constant and redirects to editConstant view.
     *
     * @param string $constantKey
     * @return ResponseInterface
     */
    protected function prepareEditConstantAction($constantKey): ResponseInterface
    {
        $this->session->setDataByKey('constantSelection', $constantKey);

        return $this->redirect('editConstant', 'EditConstant');
    }
}
