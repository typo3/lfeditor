<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\Lfeditor\Controller;

use Psr\Http\Message\ResponseInterface;
use SGalinski\Lfeditor\Exceptions\LFException;
use SGalinski\Lfeditor\Utility\Functions;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * RenameConstant controller. It contains extbase actions for RenameConstant page.
 */
class RenameConstantController extends AbstractBackendController
{
    /**
     * Opens renameConstant view.
     * It is called in 2 cases:
     * - on selection of renameConstant option in main menu,
     * - after redirection from action which must not change the view.
     *
     * @throws NoSuchCacheException
     */
    public function renameConstantAction(): ResponseInterface
    {
        $docHeaderComponent = $this->moduleTemplate->getDocHeaderComponent();

        $pageUid = $this->request->getQueryParams()['id'] ?? 0;
        /** @var BackendUserAuthentication $backendUser */
        $backendUser = $GLOBALS['BE_USER'];
        // create docheader + buttons
        $pageInfo = BackendUtility::readPageAccess($pageUid, $backendUser->getPagePermsClause(1));
        if ($pageInfo === false) {
            $pageInfo = ['uid' => $pageUid];
        }

        $docHeaderComponent->setMetaInformation($pageInfo);
        $this->docHeaderService->makeButtons($this->moduleTemplate->getDocHeaderComponent(), 'web_LfeditorGeneral');
        try {
            $this->moduleTemplate->assign('controllerName', 'RenameConstant');
            $this->prepareExtensionAndLangFileOptions();
            $this->configurationService->initFileObject(
                $this->session->getDataByKey('languageFileSelection'),
                $this->session->getDataByKey('extensionSelection')
            );
            $langData = $this->configurationService->getFileObj()->getLocalLangData();
            $constantOptions = $this->configurationService->menuConstList(
                $langData,
                LocalizationUtility::translate('select.nothing', 'lfeditor')
            );
            $this->assignViewWidthMenuVariables('constant', $constantOptions);
            $this->prepareRenameConstantViewMainSectionContent();
        } catch (LFException $e) {
            $this->addLFEFlashMessage($e);
        }

        $this->commonViewRenderingActionSettings();
        return $this->moduleTemplate->renderResponse('RenameConstant');
    }

    /**
     * This action saves in session currently selected options from selection menus in renameConstant view.
     * It is called on change of selection of any select menu in renameConstant view.
     *
     * @param string $extensionSelection
     * @param string $languageFileSelection
     * @param string $constantSelection
     * @param string $extKey
     */
    public function changeSelectionAction(
        $extensionSelection = null,
        $languageFileSelection = null,
        $constantSelection = null,
        $extKey = ''
    ): ResponseInterface {
        $this->saveSelectionsInSession(
            $extensionSelection,
            $languageFileSelection,
            null,
            $constantSelection,
            null,
            null,
            null,
            null,
            $extKey
        );

        return $this->redirect('renameConstant');
    }

    /**
     * Saves the changes made in main section of renameConstant view.
     *
     * @param string $newConstantName
     * @throws NoSuchCacheException
     */
    public function renameConstantSaveAction($newConstantName): ResponseInterface
    {
        try {
            $oldConstantName = $this->session->getDataByKey('constantSelection');
            $extConfig = $this->configurationService->getExtConfig();
            if ($oldConstantName === $newConstantName) {
                throw new LFException('failure.langfile.noChange');
            }

            $this->configurationService->initFileObject(
                $this->session->getDataByKey('languageFileSelection'),
                $this->session->getDataByKey('extensionSelection')
            );
            $langData = $this->configurationService->getFileObj()->getLocalLangData();
            $constExists = !empty($langData['default'][$newConstantName])
                || !empty($langData[$extConfig['defaultLanguage']][$newConstantName]);
            if ($constExists) {
                throw new LFException('failure.langfile.constExists');
            }

            $langArray = Functions::buildLangArray();
            $newLang = [];
            foreach ($langArray as $lang) {
                if (isset($langData[$lang][$oldConstantName])) {
                    $newLang[$lang][$newConstantName] = $langData[$lang][$oldConstantName];
                }
                $newLang[$lang][$oldConstantName] = '';
            }

            $this->configurationService->execWrite($newLang, [], true);
            $this->addFlashMessage(
                LocalizationUtility::translate('lang.file.write.success', 'lfeditor')
            );
        } catch (LFException $e) {
            $this->addLFEFlashMessage($e);
        }

        return $this->redirect('renameConstant');
    }

    /**
     * Clears extensionAndLangFileOptions cache, and in that way refreshes list of language file options in select box.
     *
     * @return ResponseInterface
     * @throws NoSuchCacheException
     */
    public function refreshLanguageFileListAction(): ResponseInterface
    {
        $this->clearSelectOptionsCache('extensionAndLangFileOptions');
        return $this->redirect('renameConstant');
    }

    /**
     * Prepares main section content of deleteConstant view.
     *
     * @throws LFException
     */
    protected function prepareRenameConstantViewMainSectionContent()
    {
        $constantSelection = $this->session->getDataByKey('constantSelection');
        if (empty($constantSelection) || $constantSelection === '###default###') {
            throw new LFException('failure.select.noConst', 1);
        }

        $this->moduleTemplate->assign('constantSelection', $constantSelection);
    }
}
