<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\Lfeditor\Controller;

use Psr\Http\Message\ResponseInterface;
use SGalinski\Lfeditor\Exceptions\LFException;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * AddConstant controller. It contains extbase actions for AddConstant page.
 */
class AddConstantController extends AbstractBackendController
{
    /**
     * Opens addConstant view.
     * It is called in 2 cases:
     * - on selection of addConstant option in main menu,
     * - after redirection from action which must not change the view.
     */
    public function addConstantAction(): ResponseInterface
    {
        $docHeaderComponent = $this->moduleTemplate->getDocHeaderComponent();
        $pageUid = $this->request->getQueryParams()['id'] ?? 0;
        /** @var BackendUserAuthentication $backendUser */
        $backendUser = $GLOBALS['BE_USER'];
        // create docheader + buttons
        $pageInfo = BackendUtility::readPageAccess($pageUid, $backendUser->getPagePermsClause(1));
        if ($pageInfo === false) {
            $pageInfo = ['uid' => $pageUid];
        }

        $docHeaderComponent->setMetaInformation($pageInfo);
        $this->docHeaderService->makeButtons($this->moduleTemplate->getDocHeaderComponent(), 'web_LfeditorGeneral');
        try {
            $this->moduleTemplate->assign('controllerName', 'AddConstant');

            $this->prepareExtensionAndLangFileOptions();
            $this->prepareAddConstantViewMainSectionContent();
        } catch (LFException|NoSuchCacheException $e) {
            $this->addLFEFlashMessage($e);
        }

        $this->commonViewRenderingActionSettings();
        return $this->moduleTemplate->renderResponse('AddConstant');
    }

    /**
     * This action saves in session currently selected options from selection menus in addConstant view.
     * It is called on change of selection of any select menu in addConstant view.
     *
     * @param string $extensionSelection
     * @param string $languageFileSelection
     * @param string $extKey
     */
    public function changeSelectionAction(
        $extensionSelection = null,
        $languageFileSelection = null,
        $extKey = ''
    ): ResponseInterface {
        $this->saveSelectionsInSession(
            $extensionSelection,
            $languageFileSelection,
            null,
            null,
            null,
            null,
            null,
            null,
            $extKey
        );

        return $this->redirect('addConstant');
    }

    /**
     * Saves the changes made in main section of addConstant view.
     *
     * @param string $nameOfConstant
     * @param array $addConstTextArea
     * @return ResponseInterface
     * @throws NoSuchCacheException
     */
    public function addConstantSaveAction(
        $nameOfConstant,
        array $addConstTextArea
    ): ResponseInterface {
        try {
            if (empty($nameOfConstant)) {
                throw new LFException('failure.select.noConstDefined');
            }
            $extConfig = $this->configurationService->getExtConfig();

            $this->configurationService->initFileObject(
                $this->session->getDataByKey('languageFileSelection'),
                $this->session->getDataByKey('extensionSelection')
            );

            $langData = $this->configurationService->getFileObj()->getLocalLangData();

            $constExists = !empty($langData['default'][$nameOfConstant])
                || !empty($langData[$extConfig['defaultLanguage']][$nameOfConstant]);
            if ($constExists) {
                throw new LFException('failure.langfile.constExists');
            }

            $newConstLanguages = [];
            foreach ($addConstTextArea as $lang => $value) {
                $newConstLanguages[$lang][$nameOfConstant] = $value;
            }

            $this->configurationService->execWrite($newConstLanguages);
            $this->session->setDataByKey('constantSelection', $nameOfConstant);

            $this->addFlashMessage(
                LocalizationUtility::translate('lang.file.write.success', 'lfeditor')
            );
        } catch (LFException $e) {
            $this->addLFEFlashMessage($e);
        }
        return $this->redirect('addConstant');
    }

    /**
     * Clears extensionAndLangFileOptions cache, and in that way refreshes list of language file options in select box.
     */
    public function refreshLanguageFileListAction(): ?ResponseInterface
    {
        $this->clearSelectOptionsCache('extensionAndLangFileOptions');
        return $this->redirect('addConstant');
    }

    /**
     * Prepares main section content of addConstant view.
     *
     * @throws LFException
     */
    protected function prepareAddConstantViewMainSectionContent()
    {
        $extConfig = $this->configurationService->getExtConfig();
        $langArray = $this->configurationService->getLangArray($this->backendUser);

        $this->moduleTemplate->assign('languages', $langArray);
        $this->moduleTemplate->assign('numTextAreaRows', $extConfig['numTextAreaRows']);
    }
}
