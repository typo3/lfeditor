<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\Lfeditor\Controller;

use Psr\Http\Message\ResponseInterface;
use SGalinski\Lfeditor\Exceptions\DirectoryAccessRightsException;
use SGalinski\Lfeditor\Exceptions\LFException;
use SGalinski\Lfeditor\Service\ConfigurationService;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * SearchConstant controller. It contains extbase actions for SearchConstant page.
 */
class SearchConstantController extends AbstractBackendController
{
    /**
     * Sets the corresponding session variables before redirecting to Edit Constant
     *
     * @param string $extensionPath
     * @param string $languageFile
     * @param string|null $constantKey
     * @param string $extKey
     * @return ResponseInterface
     */
    public function globalSearchToEditConstantAction(
        string $extensionPath = '',
        string $languageFile = '',
        string $constantKey = null,
        string $extKey = ''
    ): ResponseInterface {
        $this->saveSelectionsInSession(
            base64_decode($extensionPath),
            base64_decode($languageFile),
            null,
            null,
            null,
            null,
            null,
            null,
            $extKey
        );

        return $this->redirect('prepareEditConstant', 'EditConstant', null, ['constantKey' => $constantKey]);
    }

    /**
     * Opens Global Search view.
     * It is called in 2 cases:
     * - on selection of searchGlobal option in main menu,
     * - after redirection from action which must not change the view.
     *
     * @param bool $searchDone
     * @return ResponseInterface
     */
    public function searchGlobalAction($searchDone = false): ResponseInterface
    {
        $docHeaderComponent = $this->moduleTemplate->getDocHeaderComponent();
        $pageUid = $this->request->getQueryParams()['id'] ?? 0;
        /** @var BackendUserAuthentication $backendUser */
        $backendUser = $GLOBALS['BE_USER'];
        // create docheader + buttons
        $pageInfo = BackendUtility::readPageAccess($pageUid, $backendUser->getPagePermsClause(1));
        if ($pageInfo === false) {
            $pageInfo = ['uid' => $pageUid];
        }

        $docHeaderComponent->setMetaInformation($pageInfo);
        $this->docHeaderService->makeButtons($this->moduleTemplate->getDocHeaderComponent(), 'web_LfeditorGeneral');
        $this->moduleTemplate->assign('controllerName', 'SearchConstant');
        $this->prepareSearchConstantViewMainSectionContent($searchDone);
        $this->commonViewRenderingActionSettings();
        return $this->moduleTemplate->renderResponse('SearchGlobal');
    }

    /**
     * Saves the changes made in main section of searchGlobal view.
     *
     * @param bool $caseSensitive
     * @param string $searchStr
     * @return ResponseInterface
     * @throws DirectoryAccessRightsException
     * @throws LFException
     * @throws NoSuchCacheException
     */
    public function searchGlobalSearchAction($caseSensitive, $searchStr): ResponseInterface
    {
        $searchResultArray = [];
        $extensions = $this->getExtensions();
        if (!preg_match('/^\/.*\/.*$/', $searchStr) && !empty($searchStr)) {
            // try to speed up the process by not parsing the files at first
            if (is_callable('exec')) {
                foreach ($extensions as $extensionPath => $extensionData) {
                    if (!file_exists($extensionPath)) {
                        unset($extensions[$extensionPath]);
                        continue;
                    }

                    // always have a trailing slash while searching for compatibility reasons
                    $realExtensionPath = rtrim($extensionPath, '/') . '/';
                    $foundFiles = $this->findFiles(
                        $searchStr,
                        $realExtensionPath,
                        $extensionData['languageFileOptions'],
                        $caseSensitive
                    );
                    if (!$foundFiles) {
                        unset($extensions[$extensionPath]);
                        continue;
                    }

                    $foundFiles = array_unique($foundFiles);
                    foreach ($foundFiles as &$foundFile) {
                        $foundFile = str_replace($realExtensionPath, '', $foundFile);
                    }

                    unset($foundFile);
                    $filesToExtract = array_diff($extensionData['languageFileOptions'], $foundFiles);
                    foreach ($extensionData['languageFileOptions'] as $key => $value) {
                        foreach ($filesToExtract as $fileToExtract) {
                            if ($fileToExtract === $key) {
                                unset($extensions[$extensionPath]['languageFileOptions'][$key]);
                            }
                        }
                    }
                }
            }

            foreach ($extensions as $extensionPath => $extensionData) {
                foreach ($extensionData['languageFileOptions'] as $languageFile) {
                    $searchResultArray[$extensionData['extKey']][$languageFile] = [
                        'results' => $this->searchInFile(
                            $languageFile,
                            $extensionPath,
                            $caseSensitive,
                            $searchStr,
                            true
                        ),
                        'extensionData' => $extensionData,
                        'extensionPath' => $extensionPath,
                        'languageFile' => $languageFile,
                    ];
                }
            }

            if (empty($searchResultArray)) {
                try {
                    throw new LFException('failure.search.noConstants', 1);
                } catch (LFException $e) {
                    $this->addLFEFlashMessage($e);
                }
            }
        } else {
            try {
                throw new LFException('function.const.search.enterSearchStr', 1);
            } catch (LFException $e) {
                $this->addLFEFlashMessage($e);
            }
        }

        $this->session->setDataByKey('searchResultArray', $searchResultArray);
        $this->session->setDataByKey('searchString', $searchStr);
        $this->session->setDataByKey('searchCaseSensitive', $caseSensitive);
        return $this->redirect('searchGlobal', null, null, ['searchDone' => true]);
    }

    /**
     * Prepares main section content of searchConstant view.
     *
     * @param bool $searchDone
     */
    protected function prepareSearchConstantViewMainSectionContent($searchDone): void
    {
        $searchResultArray = [];
        if ($searchDone) {
            $searchResultArray = $this->session->getDataByKey('searchResultArray');
        } else {
            $this->session->setDataByKey('searchResultArray', []);
        }

        $searchString = $this->session->getDataByKey('searchString');
        $searchCaseSensitive = $this->session->getDataByKey('searchCaseSensitive');
        $this->moduleTemplate->assign('searchResultArray', $searchResultArray);
        $this->moduleTemplate->assign('searchString', $searchString);
        $this->moduleTemplate->assign('searchCaseSensitive', $searchCaseSensitive);
    }

    /**
     * Uses grep in the shell to find and filter the files that need to be parsed
     *
     * @param $searchString
     * @param $extensionRootPath
     * @param $extensionLangFiles
     * @param $isCaseSensitive
     * @return array|false
     */
    protected function findFiles($searchString, $extensionRootPath, $extensionLangFiles, $isCaseSensitive)
    {
        if (empty($extensionLangFiles)) {
            return false;
        }

        $result = [];
        foreach ($extensionLangFiles as $extensionLangFile) {
            $path = $extensionRootPath . dirname($extensionLangFile) . '/*' . basename($extensionLangFile);
            // Escape the search string to prevent shell injection
            $escapedSearchString = escapeshellarg($searchString);
            // Use the grep command to search for the string in files
            $grepCommand = 'grep -l ';
            if (!$isCaseSensitive) {
                $grepCommand .= ' -i ';
            }

            $grepCommand .= "-r {$escapedSearchString} {$path}";
            // Execute the grep command and capture the output
            exec($grepCommand, $output, $returnCode);
            // Check if the grep command executed successfully
            if ($returnCode === 0 && !empty($output)) {
                /** @noinspection SlowArrayOperationsInLoopInspection */
                $result = array_merge($result, [$extensionLangFile]);
            }
        }

        return $result;
    }

    /**
     * Parses the XLF file and searches for the string in it
     *
     * @param string $languageFileSelection
     * @param string $extensionSelection
     * @param bool $caseSensitive
     * @param string $searchStr
     * @param bool $noErrors
     * @return array
     * @throws DirectoryAccessRightsException
     */
    protected function searchInFile(
        string $languageFileSelection,
        string $extensionSelection,
        bool $caseSensitive,
        string $searchStr,
        bool $noErrors
    ): array {
        $searchResultArray = [];
        try {
            $configurationService = GeneralUtility::makeInstance(ConfigurationService::class);
            $configurationService->prepareConfig();
            $configurationService->initFileObject(
                $languageFileSelection,
                $extensionSelection
            );
            $langData = $configurationService->getFileObj()->getLocalLangData();
            $viewLanguages = $configurationService->getLangArray($this->backendUser);
            $searchOptions = $caseSensitive ? '' : 'i';
            if (!empty($searchStr) && !preg_match('/^\/.*\/.*$/', $searchStr)) {
                foreach ($viewLanguages as $langKey) {
                    if (!array_key_exists($langKey, $langData) || !is_array($langData[$langKey])) {
                        continue;
                    }

                    foreach ($langData[$langKey] as $labelKey => $labelValue) {
                        $labelKeyOrValueContainSearchStr = preg_match(
                            '/' . $searchStr . '/' . $searchOptions,
                            $labelKey
                        ) || preg_match('/' . $searchStr . '/' . $searchOptions, $labelValue);
                        if ($labelKeyOrValueContainSearchStr) {
                            $searchResultArray[$langKey][$labelKey] = $labelValue;
                        }
                    }
                }

                if (!count($searchResultArray) && !$noErrors) {
                    throw new LFException('failure.search.noConstants', 1);
                }
            } elseif (!$noErrors) {
                throw new LFException('function.const.search.enterSearchStr', 1);
            }
        } catch (LFException $e) {
            $this->addLFEFlashMessage($e);
        }

        return $searchResultArray;
    }
}
