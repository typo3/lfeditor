<?php

$EM_CONF['lfeditor'] = [
    'title' => 'Language File Editor',
    'description' => 'This module serves several functions for editing of language files.
The extension differs between editors and administrators with appropriate privileges.
Normal users are allowed to edit only languages for which they have permission.
Following functions are implemented in this module:
	* Formats: PHP, XML and XLF
	* Conversion of formats
	* Splitting and merging of language files
	* Override mode
	* Editing L10n folder content
	* Simple editing of constants and languages (edit/add/rename/delete constants)
	* View of constants and values
	* Tree view of constants
	* Meta information handling
	* Global search',
    'category' => 'module',
    'version' => '8.2.1',
    'state' => 'stable',
    'author' => 'Stefan Galinski, Damjan Komlenac',
    'author_email' => 'stefan@sgalinski.de',
    'author_company' => 'sgalinski Internet Services',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-12.4.99',
            'php' => '8.2.1-8.3.99',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
];
