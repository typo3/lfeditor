<?php

use SGalinski\Lfeditor\Utility\ExtensionUtility;
use TYPO3\CMS\Core\Core\Environment;

if (!defined('TYPO3')) {
    die('Access denied.');
}

// new cache table
if (!isset($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['lfeditor_select_options_cache'])) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['lfeditor_select_options_cache'] = [];
}

$extConf = ExtensionUtility::getExtensionConfiguration();
$projectPath = Environment::getProjectPath() . '/';
if (
    array_key_exists('pathAdditionalConfiguration', $extConf) &&
    $extConf['pathAdditionalConfiguration'] !== 'typo3conf/AdditionalConfiguration.php' &&
    is_file($projectPath . $extConf['pathAdditionalConfiguration'])
) {
    // Include the additional LFEditor configuration file if not set to AdditionalConfiguration.php
    include_once $projectPath . $extConf['pathAdditionalConfiguration'];
}

unset($extConf, $pathSite);
